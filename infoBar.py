# Declare Import
import tkinter as tk
import yaml

# Declare Var
target = 'No target'
path_file = '/home/automation/.config/target/target.yaml'

def readYaml():
  try: 
    with open(path_file, 'r') as file:
        data = yaml.safe_load(file)
        if 'target' in data:
          if data['target'] != '':
            return data['target']
          else:
            return None
        else:
          return None
  except FileNotFoundError:
      print('The file not found.')
      return None

def updateLabel():
  targetYaml = readYaml()
  if targetYaml is not None:
      label.config(text="Target: "+targetYaml)
  window.after(10000, updateLabel)

window = tk.Tk()
window.title('Info Target')
label = tk.Label(text="Target: "+target)
label.pack()

updateLabel()

window.mainloop()